<?php
/*
Template Name: CW Gallery
*/
get_header(); ?>

<!-- Here starts the code for the main body of the gallery. -->
<div class="row">
  <div class="small-12 columns">
    <!-- *** GALLERY
================================================================================= -->
    <div class="gallery">
      <div class="gallery-item">
        <div class="image-before"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/laron-before-e1497462007454-960x960.jpg" /></div>
        <div class="image-after"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/laron-after-960x960.jpg" /></div>
      </div>
      <!-- gallery-item 1-->

      <div class="gallery-item">
        <div class="image-before"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/KERVAN-BEFORE-960x960.jpg" /></div>
        <div class="image-after"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/KERVAN-AFTER-960x960.jpg" /></div>
      </div>
      <!-- gallery-item 2-->


      <div class="gallery-item">
        <div class="image-before"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/fournet-scar-before-960x960.jpg" /></div>
        <div class="image-after"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/fournet-scar-after-960x960.jpg" /></div>
      </div>
      <!-- gallery-item 3-->


      <div class="gallery-item">
        <div class="image-before"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/darius-back-before-960x960.jpg" /></div>
        <div class="image-after"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/darius-back-after-960x960.jpg" /></div>
      </div>
      <!-- gallery-item 4-->


      <div class="gallery-item">
        <div class="image-before"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/corey-crop-before-960x960.jpg" /></div>
        <div class="image-after"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/corey-crop-after-960x960.jpg" /></div>
      </div>
      <!-- gallery-item 5-->


      <div class="gallery-item">
        <div class="image-before"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/aurelio-before-crop-960x960.jpg" /></div>
        <div class="image-after"><img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/aurelio-after-crop-960x960.jpg" /></div>
      </div>
      <!-- gallery-item 6-->

      <!-- Whoops, added more. Good template to leave in. 
<div class="gallery-item clearfix">
<div class="img-before"><p class="h3 img-title text-upper text-center">Before</p><img width="450" height="450" src=""</div>
<div class="img-after"><p class="h3 img-title text-upper text-center">After</p><img width="450" height="450" src=""</div>
</div><!-- gallery-item 7
<!-- gallery -->
    </div>

    <!-- ***  GALLERY NAVIGATION
================================================================================= -->
    <div class="gallery-nav">
      <div class="gallery-thumb">
        <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/laron-after-150x150.jpg" />
      </div>
      <!-- gallery-thumb 1-->

      <div class="gallery-thumb">
        <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/KERVAN-AFTER-150x150.jpg" />
      </div>

      <div class="gallery-thumb">
        <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/fournet-scar-after-150x150.jpg" />
      </div>

      <div class="gallery-thumb">
        <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/darius-back-after-150x150.jpg" />
      </div>
      <!-- gallery-thumb 4-->

      <div class="gallery-thumb">
        <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/corey-crop-after-150x150.jpg" />
      </div>
      <!-- gallery-thumb 5-->

      <div class="gallery-thumb">
        <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/aurelio-after-crop-150x150.jpg" />
      </div>
      <!-- gallery-thumb 6-->

    </div>
  </div>
</div>

<!-- needed for slick. -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

<?php get_footer(); ?>