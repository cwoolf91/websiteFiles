jQuery(document).ready(function($) {
  // Gallery
  jQuery('.gallery').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    focusOnSelect: true,
    variableWidth: false,
    asNavFor: '.gallery-nav',
    adaptiveHeight: false,
    initialSlide: 0,
    arrows: true,
    fade: true,
    useTransform: true
  });

  jQuery('.gallery-nav').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.gallery',
    dots: false,
    focusOnSelect: true,
    variableWidth: false,
    adaptiveHeight: false,
    initialSlide: 0,
    arrows: true,
    useTransform: true,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 5,
      }
    }, {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
      }
    }, {
      breakpoint: 767,
      settings: {
        slidesToShow: 3,
      }
    }, {
      breakpoint: 569,
      settings: {
        slidesToShow: 2,
      }
    }, {
      breakpoint: 360,
      settings: {
        slidesToShow: 1,
      }
    }]
  });
  
  jQuery('.gallery').on('afterChange', function(event, slick, currentSlide){
  	console.log('testing function.')
  });
});
