
<div class="row">
      <script>
jQuery(document).ready(function($) {
  // Gallery
  jQuery('.gallery').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    centerPadding: '5px',
    centerMode: true,
    focusOnSelect: true,
    variableWidth: false,
    asNavFor: '.gallery-nav',
    adaptiveHeight: true,
    initialSlide: 0,
    arrows: true,
    fade: true,
    useTransform: true
  });

  jQuery('.gallery-nav').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.gallery',
    dots: false,
    focusOnSelect: true,
    variableWidth: false,
    adaptiveHeight: false,
    initialSlide: 0,
    arrows: true,
    useTransform: true
      /*responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 7,
        }
      }, {
        breakpoint: 991,
        settings: {
          slidesToShow: 5,
        }
      }, {
        breakpoint: 767,
        settings: {
          slidesToShow: 4,
        }
      }, {
        breakpoint: 569,
        settings: {
          slidesToShow: 3,
        }
      }, {
        breakpoint: 360,
        settings: {
          slidesToShow: 2,
        }
      }]*/
  });

  /* adding active class 
  jQuery('.gallery').on('afterChange', function(event, slick, direction) {
    jQuery('.gallery-nav .slick-active:eq(' + slick.currentSlide + ')').addClass('slick-current').siblings().removeClass('slick-current');
  });*/


});

</script>
  <div class="gallery">
    <div class="gallery-item">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/laron-before-e1497462007454-960x960.jpg" />
    </div>
    <!-- gallery-item 1-->

    <div class="gallery-item">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/KERVAN-BEFORE-960x960.jpg" />
    </div>
    <!-- gallery-item 2-->


    <div class="gallery-item">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/fournet-scar-before-960x960.jpg" />
    </div>
    <!-- gallery-item 3-->


    <div class="gallery-item">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/darius-back-before-960x960.jpg" />
    </div>
    <!-- gallery-item 4-->


    <div class="gallery-item">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/corey-crop-before-960x960.jpg" />
    </div>
    <!-- gallery-item 5-->


    <div class="gallery-item">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/aurelio-before-crop-960x960.jpg" />
    </div>
  </div>

  <!-- ***  GALLERY NAVIGATION
						================================================================================= -->
  <div class="gallery-nav">
    <div class="gallery-thumb">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/laron-after-150x150.jpg" />
    </div>
    <!-- gallery-thumb 1-->

    <div class="gallery-thumb">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/KERVAN-AFTER-150x150.jpg" />
    </div>

    <div class="gallery-thumb">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/fournet-scar-after-150x150.jpg" />
    </div>

    <div class="gallery-thumb">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/darius-back-after-150x150.jpg" />
    </div>
    <!-- gallery-thumb 4-->

    <div class="gallery-thumb">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/corey-crop-after-150x150.jpg" />
    </div>
    <!-- gallery-thumb 5-->

    <div class="gallery-thumb">
      <img src="https://1bravi.robidev.com/wp-content/uploads/Gallery/aurelio-after-crop-150x150.jpg" />
    </div>
    <!-- gallery-thumb 6-->
  </div>
</div>
